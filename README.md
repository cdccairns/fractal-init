# fractal-init

## Usage

This repository is intended to act as a source directory for the development and documentation of a component library using [Fractal](https://fractal.build). The component library can be run as a local development server and/or exported to static HTML.

## Installation

> The following instructions presume the existence of a project subdirectory into which the repository can be cloned, and an instance of `package.json` located in a project root directory.

In your project subdirectory, run `$ git clone <repository> <directory>` and `$ cd <directory> rm -rf .git` to clone the repository and delete its commit history.

Then, run `$ npm install --save-dev @frctl/fractal @frctl/twig` to install [Fractal](https://www.npmjs.com/package/@frctl/fractal) and the [Fractal Twig template adapter](https://www.npmjs.com/package/@frctl/twig).

Add the following lines to `package.json`, replacing `<directory>` with the relative path to your Fractal subdirectory (where `fractal.config.js` is located).

```
"scripts": {
    "fractal:build": "cd <directory> && fractal build",
    "fractal:start": "cd <directory> && fractal start"
}
```

Run `$ npm run fractal:start` to start the local development server or `$ npm run fractal:build` to export to static HTML.

> Before exporting to static HTML, set the [build destination](https://fractal.build/guide/web/exporting-static-html.html#configuration) in `fractal.config.js`.

### Static assets

By default Fractal is configured to look for static assets (stylesheets, JavaScript files, etc.) in the directory `public`. To specify a different path, for example to a project's distribution directory, amend the value of `static.path` in `project.config.js`:

```
fractal.web.set('static.path', <directory>);
```
