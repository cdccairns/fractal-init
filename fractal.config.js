'use strict';

const fractal = module.exports = require('@frctl/fractal').create();

/**
 * Use Twig template engine adapter for components.
 * @link https://github.com/frctl/fractal/tree/main/packages/twig
 */

const twig = require('@frctl/twig');
const adapter = twig({
    handlePrefix: '%',
    base: '/'
});

fractal.components.engine(adapter);
fractal.components.set('ext', '.twig');


/**
 * Set file paths.
 */

fractal.components.set('path', `${__dirname}/templates`);
fractal.docs.set('path', `${__dirname}/pages`);
fractal.web.set('static.path', `${__dirname}/public`);


/**
 * Set project labels.
 */

const labels = {};
labels.project = 'fractal-init';
labels.components = 'Templates';

fractal.set('project.title', labels.project);
fractal.components.set('label', labels.components);
fractal.components.set('title', labels.components);


/**
 * Configure theme.
 * @link https://fractal.build/guide/web/default-theme.html#configuration
 */

const mandelbrot = require('@frctl/mandelbrot');
const theme = mandelbrot({
  nav: ['docs', 'components'],
  panels: ['view', 'html', 'notes'],
  skin: 'white'
});

fractal.web.theme(theme);


/**
 * Configure development server.
 * @link https://fractal.build/guide/web/configuration-reference.html#server-sync
 */

fractal.web.set('server.sync', true);
fractal.web.set('server.syncOptions', {
  port: 4000,
  notify: false,
  open: true,
  ui: false
});
